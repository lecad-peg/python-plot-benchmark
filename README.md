# Python plot benchmark between matplotlib and pyqtgraph #

This repository hold scripts for testing matplotlib and pyqtgraph capabilities in terms of loading speed and
interactivity performance.

### What is this repository for? ###

This repository is for showing the pros/cons of matplotlib and pyqtgraph in terms of

* Use case complexity (how easy/hard is to plot with it)
* Loading and rendering performance
* Ease of integrating into Graphical User interfaces

![Overview](images/matplotlib_and_pyqtgraph_test_GUI_overview_850x.png)

Fig. 1: Overview


### Requirements ###

[Python 3.6+](https://www.python.org/) is recommended. Install [pyqtgraph](https://github.com/pyqtgraph/pyqtgraph) and [matplotlib](https://matplotlib.org/users/installing.html) with the use of PIP. Below are listed the PIP commands for installing the packages together with package versions which were used in the latest developments.

```console
$ pip install matplotlib  # recommended version: 3.1.1
$ pip install pyqtgraph   # recommended version: 0.11.0rc0
```

---
**NOTE**

If you use Microsoft Windows and have Python 3.8 installed, then you have to install pyqtgraph-0.11.0rc0 as version 0.10.0 will not work. In this case you can install it by manually downloading the package from pyqtgraph [releases](https://github.com/pyqtgraph/pyqtgraph/releases).
But if you have git installed you can then use PIP to install the latest version with the following command in Windows cmd terminal.

```console
$ pip3 install --upgrade git+http://github.com/pyqtgraph/pyqtgraph.git
```

---

### Unit testing ###

The validation and performance check of the code is done with [unittest](https://docs.python.org/3/library/unittest.html).

**Requirements:**

```console
$ pip install imageHash # recommended version: 4.1.0
$ pip install Pillow    # recommended version: 7.2.0
```

To run the tests navigate to the ``test`` directory and run:

```console
$ python -m unittest -v matplotlib_unittest -b
$ python -m unittest -v pyqtgraph_unittest -b
```

For each test the time required and success state is being displayed. Example of the unit test output (done on Dell Precision 5520; Intel(R) Core(TM) i5-7300HQ CPU @ 2.50GHz, 16GB RAM, OS: Ubuntu 19.10):

```console
# matplotlib_unittest result
test_figureCheck (matplotlib_unittest.TestMatplotlib) ... 0.483s | ok
test_numberOfPlots (matplotlib_unittest.TestMatplotlib) ... 0.001s | ok
test_numberOfPointsPerPlot (matplotlib_unittest.TestMatplotlib) ... 0.000s | ok
test_plot (matplotlib_unittest.TestMatplotlib) ... 0.227s | ok
test_selectFunction (matplotlib_unittest.TestMatplotlib) ... 0.000s | ok
test_verticalDisplacementVector (matplotlib_unittest.TestMatplotlib) ... 0.000s | ok

----------------------------------------------------------------------
Ran 6 tests in 0.714s

OK
```

```console
# pyqtgraph_unittest result
test_figureCheck (pyqtgraph_unittest.TestPyqtgraph) ... 1.739s | ok
test_numberOfPlots (pyqtgraph_unittest.TestPyqtgraph) ... 0.000s | ok
test_numberOfPointsPerPlot (pyqtgraph_unittest.TestPyqtgraph) ... 0.000s | ok
test_plot (pyqtgraph_unittest.TestPyqtgraph) ... 0.132s | ok
test_selectFunction (pyqtgraph_unittest.TestPyqtgraph) ... 0.000s | ok
test_verticalDisplacementVector (pyqtgraph_unittest.TestPyqtgraph) ... 0.000s | ok

----------------------------------------------------------------------
Ran 6 tests in 1.875s

OK
```

### Comparison of matplotlib VS pyqtgraph ###

![10 sin plots each with 100000 nodes](images/test_10_100000_sin_2_850x.png)

Fig. 2: Matplotlib and PyQtGraph comparison: 10 sin plots each with 100000 nodes.

![100 sin plots each with 100000 nodes](images/test_100_100000_sin_2_850x.png)

Fig. 3: Matplotlib and PyQtGraph comparison: 100 sin plots each with 100000 nodes.

![10 sin plots each with 100000 nodes. PyQtgraph with prettied lines (width=3)](images/test_10_100000_sin_2_pyqtgraph_linewidth_3_850x.png)

Fig. 4: Matplotlib and PyQtGraph (line width = 3) comparison: 10 sin plots each with 100000 nodes.

While running the above examples the next properties were observed:

* PyQtGraph (with default line width) has greater interactivity performance while matplotlib plots faster.
* for number of plots less than 200, PyQtGraph (default line width) plots slightly faster than matplotlib
* By setting wider lines in PyQtGraph it loses all interactivity performance advantage and further increases time of plotting.
* PyQtGraph does not offer any interactive way of changing plot lines color, width etc. while matplotlib does

### Conclusions ###

The total time required for plotting (preparing data +
rendering) is comparable for both tools. It was observed that
while PyQtGraph performs fairly well with the default line
width set, its performance greatly decreases when the line width is
being changed while matplotlib performance remained steady and overall better
than the one of PyQtGraph.

Following the above and the fact, that matplotlib is more used by
the user community and it is being more actively developed and supported than
PyQtGraph, matplotlib is being suggested as a superior alternative for
developing a general plotting utilities
in Python3.









