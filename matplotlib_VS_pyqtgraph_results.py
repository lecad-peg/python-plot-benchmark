from pyqtgraph.Qt import QtGui, QtCore
from PyQt5 import QtGui, QtCore, QtWidgets
import numpy as np
import pyqtgraph as pg

n_plots = [1, 10, 25, 50, 75, 100, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1000]

"""
pyqtgraph
legend = OFF
antialiasing = OFF
x = 100
y = 100

display time: instant
interactivity (zooming etc.) - 1000 plots: excellent, instant
"""
t_100x100_pyqtgraph_lOFF_aOFF = [0.0028121471405029297,
                                 0.035584449768066406,
                                 0.07712674140930176,
                                 0.2031259536743164,
                                 0.40593957901000977,
                                 0.6578469276428223,
                                 1.2958478927612305,
                                 2.2031190395355225,
                                 4.693875312805176,
                                 8.030676126480103,
                                 12.374661207199097,
                                 17.302443265914917,
                                 22.501730918884277,
                                 29.189345836639404,
                                 36.67589712142944,
                                 45.426926612854004]

"""
PyQtgraph:
legend = ON
antialiasing = OFF
x = 100
y = 100

display time: instant
interactivity (zooming etc.) - 1000 plots: very good, almost instant
"""
t_100x100_pyqtgraph_lON_aOFF = [0.002916574478149414,
                                0.043019771575927734,
                                0.10426139831542969,
                                0.27194690704345703,
                                0.5530779361724854,
                                0.9303216934204102,
                                1.9109253883361816,
                                3.3556511402130127,
                                7.200452566146851,
                                12.402027606964111,
                                18.976176500320435,
                                27.389039754867554,
                                36.626867055892944,
                                45.32786583900452,
                                56.98799276351929,
                                72.98535132408142]

"""
matplotlib
legend = ON
legend loc = best (decreases performance)
x = 100
y = 100

display time: for 1000 plots takes additional 5-6 seconds to display the plot!
interactivity (zooming etc.) - 1000 plots: extremely slow, for every action it
takes more than 20+ seconds (up to a minute and more) to take effect
(update view etc.)
"""

t_100x100_matplotlib_lON_best = [0.047403573989868164,
                                 0.09131288528442383,
                                 0.158278226852417,
                                 0.2844698429107666,
                                 0.4961512088775635,
                                 0.6662182807922363,
                                 0.9897668361663818,
                                 1.2244513034820557,
                                 1.8625104427337646,
                                 2.46989369392395,
                                 3.157086133956909,
                                 3.7505669593811035,
                                 4.424193859100342,
                                 5.171892404556274,
                                 5.880699157714844,
                                 6.407812595367432]

"""
pyqtgraph
legend = ON
antialiasing = ON
x = 1000
y = 1000

display time: instant
interactivity (zooming etc.) - 1000 plots: decent, for every action it takes
1-2 seconds to take effect (update view etc.)
"""
t_1000x1000_pyqtgraph_lON_aON = [0.003011941909790039,
                                 0.034018516540527344,
                                 0.10488438606262207,
                                 0.2759065628051758,
                                 0.5556001663208008,
                                 0.9696152210235596,
                                 1.9721145629882812,
                                 3.334735870361328,
                                 7.168520212173462,
                                 12.42306137084961,
                                 19.03852939605713,
                                 27.46252989768982,
                                 36.67490744590759,
                                 47.624823808670044,
                                 60.61382579803467,
                                 74.66963195800781]

"""
pyqtgraph
legend = OFF
antialiasing = OFF
x = 1000
y = 1000

display time: instant
interactivity (zooming etc.) - 1000 plots: decent, for every action it takes
1-2 seconds to take effect (update view etc.)
"""
t_1000x1000_pyqtgraph_lOFF_aOFF =  [0.0022678375244140625,
                                    0.051384925842285156,
                                    0.08617115020751953,
                                    0.21413350105285645,
                                    0.4105501174926758,
                                    0.6779360771179199,
                                    1.3187143802642822,
                                    2.2885515689849854,
                                    4.726994514465332,
                                    8.225724697113037,
                                    12.655046463012695,
                                    18.28031635284424,
                                    24.210349082946777,
                                    31.28221035003662,
                                    39.00025033950806,
                                    49.00182890892029]

"""
matplotlib
legend = ON
legend loc = best (decreases performance)
x = 1000
y = 1000

interactivity (zooming etc.) - 1000 plots: extremely slow, for every action
it takes more than 20+ seconds (up to a minute and more) to take effect (update view etc.)
"""

t_1000x1000_matplotlib_lON_best =  [0.06522750854492188,
                                    0.19273829460144043,
                                    0.38776612281799316,
                                    0.742966890335083,
                                    1.2008044719696045,
                                    1.5542237758636475,
                                    2.1914472579956055,
                                    2.9262588024139404,
                                    4.433118104934692,
                                    5.667330265045166,
                                    7.151259899139404,
                                    8.96366000175476,
                                    10.364521741867065,
                                    11.562037229537964,
                                    13.66512155532837,
                                    14.656655550003052]


"""
matplotlib
legend = ON
legend loc = upper right
x = 1000
y = 1000

display time: for 1000 plots takes additional 10-20 seconds to display the plot!
interactivity (zooming etc.) - 1000 plots: extremely slow, for every action
it takes more than 20+ seconds (up to a minute and more) to take effect (update view etc.)
"""

t_1000x1000_matplotlib_lON_ur = [0.06342005729675293,
                                 0.18602752685546875,
                                 0.3875236511230469,
                                 0.6946876049041748,
                                 1.0981366634368896,
                                 1.5217530727386475,
                                 2.1805412769317627,
                                 2.8267769813537598,
                                 4.320133686065674,
                                 5.677379608154297,
                                 7.0373694896698,
                                 8.567216157913208,
                                 9.883590698242188,
                                 10.900628566741943,
                                 12.935705423355103,
                                 14.31503415107727]

"""
matplotlib
legend = OFF
x = 1000
y = 1000

display time: for 1000 plots takes additional 5-10 seconds to display the plot!
interactivity (zooming etc.) - 1000 plots: extremely slow, for every action
it takes more than 20+ seconds (up to a minute and more) to take effect (update view etc.)
"""

t_1000x1000_matplotlib_lOFF =  [0.06145787239074707,
                                0.16939520835876465,
                                0.3316621780395508,
                                0.6530492305755615,
                                0.9127218723297119,
                                1.1302556991577148,
                                1.732776165008545,
                                2.0987236499786377,
                                3.349632740020752,
                                4.362128257751465,
                                5.305332660675049,
                                6.281878232955933,
                                7.661864280700684,
                                8.538803339004517,
                                9.449374675750732,
                                10.402729272842407]

app = QtGui.QApplication([])

mw1 = QtWidgets.QMainWindow()
mw2 = QtWidgets.QMainWindow()

# Enable antialiasing for prettier plots
pg.setConfigOptions(antialias=True)
# Set default background color: white
pg.setConfigOption('background', 'w')
# Set default foreground (text etc.) color: black
pg.setConfigOption('foreground', 'k')

p1 = pg.PlotWidget(title=" 100 x values, 100 y values")
p1.addLegend()
p1.plot(n_plots, t_100x100_pyqtgraph_lOFF_aOFF, pen=pg.mkPen(width=3, color=(255, 0, 0)), name="PyQtGraph legend=OFF antialiasing=OFF")
p1.plot(n_plots, t_100x100_pyqtgraph_lON_aOFF, pen=pg.mkPen(width=3, color=(0, 255, 0)), name="PyQtGraph legend=ON antialiasing=OFF")
p1.plot(n_plots, t_100x100_matplotlib_lON_best, pen=pg.mkPen(width=3, color=(0, 0, 255)), name="Matplotlib legend=ON loc='best'")
p1.setLabel('bottom', "Number of plots")
p1.setLabel('left', "Time", units='s')

p2 = pg.PlotWidget(title=" 1000 x values, 1000 y values")
p2.addLegend()
p2.plot(n_plots, t_1000x1000_pyqtgraph_lON_aON, pen=pg.mkPen(width=3, color=(255, 0, 0)), name="PyQtGraph legend=ON antialiasing=ON")
p2.plot(n_plots, t_1000x1000_pyqtgraph_lOFF_aOFF, pen=pg.mkPen(width=3, color=(0, 255, 0)), name="PyQtGraph legend=OFF antialiasing=OFF")
p2.plot(n_plots, t_1000x1000_matplotlib_lON_best, pen=pg.mkPen(width=3, color=(0, 0, 255)), name="Matplotlib legend=ON loc='best'")
p2.plot(n_plots, t_1000x1000_matplotlib_lON_ur, pen=pg.mkPen(width=3, color=(255, 100, 255)), name="Matplotlib legend=ON loc='upper right'")
p2.plot(n_plots, t_1000x1000_matplotlib_lOFF, pen=pg.mkPen(width=3, color=(0, 100, 0)), name="Matplotlib legend=OFF")
p2.setLabel('bottom', "Number of plots")
p2.setLabel('left', "Time", units='s')

mw1.setCentralWidget(p1)
mw2.setCentralWidget(p2)
mw1.show()
mw2.show()


## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
