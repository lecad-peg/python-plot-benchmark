from PyQt5.QtWidgets import (QMainWindow, QWidget, QVBoxLayout, QPushButton,
                             QGroupBox, QFormLayout, QSpinBox, QComboBox)
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as \
    NavigationToolbar
from matplotlib.figure import Figure
import numpy as np
from time import time
import sys
import matplotlib
matplotlib.use('Qt5Agg')


class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)


class PlotSettings(QGroupBox):
    def __init__(self, parent=None):
        super(PlotSettings, self).__init__(parent)
        self.setTitle('Plot settings')

        layout = QFormLayout()

        self.numberOfPlots = QSpinBox()
        self.numberOfPlots.setRange(1, 100)
        self.numberOfPlots.setSingleStep(1)
        self.numberOfPlots.setValue(10)
        layout.addRow('Number of plots: ', self.numberOfPlots)

        self.numberOfPointsPerPlot = QSpinBox()
        self.numberOfPointsPerPlot.setRange(10, 10_000_000)
        self.numberOfPointsPerPlot.setSingleStep(100)
        self.numberOfPointsPerPlot.setValue(100000)
        layout.addRow('Number of points per plot: ', self.numberOfPointsPerPlot)

        self.selectFunction = QComboBox()
        self.selectFunction.addItems(['sin', 'exp', 'random', 'constant'])
        layout.addRow('Function to display', self.selectFunction)

        self.verticalDisplacementFactor = QSpinBox()
        self.verticalDisplacementFactor.setRange(1, 10)
        self.verticalDisplacementFactor.setSingleStep(1)
        self.verticalDisplacementFactor.setValue(2)
        layout.addRow('Vertical space between plots',
                      self.verticalDisplacementFactor)

        self.setLayout(layout)


class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        # Create the maptlotlib FigureCanvas object,
        # which defines a single set of axes as self.axes.
        self.canvas = MplCanvas(self, width=5, height=4, dpi=100)
        widget = QWidget(self)

        self.toolbar = NavigationToolbar(self.canvas, self)

        plotButton = QPushButton("Plot")
        plotButton.clicked.connect(self.plotData)

        self.plotSettings = PlotSettings()

        layout = QVBoxLayout(widget)

        layout.addWidget(self.canvas)
        layout.addWidget(self.toolbar)
        layout.addWidget(self.plotSettings)
        layout.addWidget(plotButton)
        widget.setLayout(layout)

        self.setCentralWidget(widget)
        self.setSettings()

        self.show()

    def setSettings(self):
        self.n_plots = self.plotSettings.numberOfPlots.value()
        self.n_points = self.plotSettings.numberOfPointsPerPlot.value()
        self.vertical_f = self.plotSettings.verticalDisplacementFactor.value()
        self.function = self.plotSettings.selectFunction.currentIndex()

    def clearPlot(self):
        self.canvas.axes.clear()

    def plotData(self):
        self.clearPlot()
        print("plotData START")

        x = np.linspace(0, 12 * np.pi, self.n_points)
        for i in range(self.n_plots):
            if self.function == 0:
                y = np.sin(x) + i * self.vertical_f
            elif self.function == 1:
                y = np.exp(x) + i * self.vertical_f
            elif self.function == 2:
                y = np.random.random(x.shape) + i * self.vertical_f
            else:
                y = i * self.vertical_f * np.ones(x.shape)
            self.canvas.axes.plot(x, y, label=f"plot {i}")
        # self.canvas.axes.legend()
        self.canvas.axes.legend(loc='upper right')
        self.canvas.axes.set_title("Matplotlib example plot")

        t1 = time()
        self.canvas.draw()
        t2 = time()
        print("Number of plots: ", self.n_plots)
        print(f"time: {t2-t1}s")

    def saveFig(self, filename):
        self.canvas.print_figure(filename, format="png")


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    app = QApplication(sys.argv)
    w = MainWindow()
    w.plotData()
    # w.saveFig("benchmarkfigure_matplotlib.png")
    w.show()
    app.exec_()
