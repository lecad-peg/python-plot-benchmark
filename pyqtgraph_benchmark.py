from PyQt5.QtWidgets import (QMainWindow, QWidget, QVBoxLayout, QPushButton,
                             QGroupBox, QFormLayout, QSpinBox, QCheckBox,
                             QComboBox)
import pyqtgraph as pg
import pyqtgraph.exporters
import numpy as np
from time import time
import sys
import matplotlib
matplotlib.use('Qt5Agg')


class PlotSettings(QGroupBox):
    def __init__(self, parent=None):
        super(PlotSettings, self).__init__(parent)
        self.setTitle('Plot settings')

        layout = QFormLayout()

        self.numberOfPlots = QSpinBox()
        self.numberOfPlots.setRange(1, 100)
        self.numberOfPlots.setSingleStep(1)
        self.numberOfPlots.setValue(10)
        layout.addRow('Number of plots: ', self.numberOfPlots)

        self.numberOfPointsPerPlot = QSpinBox()
        self.numberOfPointsPerPlot.setRange(10, 10_000_000)
        self.numberOfPointsPerPlot.setSingleStep(100)
        self.numberOfPointsPerPlot.setValue(100000)
        layout.addRow('Number of points per plot: ', self.numberOfPointsPerPlot)

        self.verticalDisplacementFactor = QSpinBox()
        self.verticalDisplacementFactor.setRange(1, 10)
        self.verticalDisplacementFactor.setSingleStep(1)
        self.verticalDisplacementFactor.setValue(2)
        layout.addRow('Vertical space between plots',
                      self.verticalDisplacementFactor)

        self.selectFunction = QComboBox()
        self.selectFunction.addItems(['sin', 'exp', 'random', 'constant'])
        layout.addRow('Function to display', self.selectFunction)

        self.antialiasOn = QCheckBox()
        self.antialiasOn.setChecked(True)
        layout.addRow('Antialias', self.antialiasOn)
        self.setLayout(layout)


class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        # Create the maptlotlib FigureCanvas object,
        # which defines a single set of axes as self.axes.
        widget = QWidget()

        plotButton = QPushButton("Plot")
        plotButton.clicked.connect(self.plotData)

        self.plotSettings = PlotSettings()

        layout = QVBoxLayout(widget)

        # PyQtGraph
        pg.setConfigOptions(antialias=True)
        # # Set default background color: white
        # pg.setConfigOption('background', 'w')
        # # Set default foreground (text etc.) color: black
        # pg.setConfigOption('foreground', 'k')

        self.pltWidget = pg.PlotWidget()
        self.pltWidget.setTitle('PyQtgraph example plot')
        self.pltWidget.addLegend()
        self.plotItemContainer = [None for i in range(100)]

        layout.addWidget(self.pltWidget)

        layout.addWidget(self.plotSettings)
        layout.addWidget(plotButton)
        widget.setLayout(layout)

        self.setCentralWidget(widget)

        self.setSettings()
        self.show()

    def setSettings(self):
        self.n_plots = self.plotSettings.numberOfPlots.value()
        self.n_points = self.plotSettings.numberOfPointsPerPlot.value()
        self.vertical_f = self.plotSettings.verticalDisplacementFactor.value()
        self.function = self.plotSettings.selectFunction.currentIndex()
        pg.setConfigOptions(antialias=self.plotSettings.antialiasOn.isChecked())

    def clearPlot(self):
        pass

    def plotData(self):
        self.clearPlot()
        print("plotData START")

        x = np.linspace(0, 12 * np.pi, self.n_points)
        t1 = time()
        for i in range(self.n_plots):
            if self.function == 0:
                y = np.sin(x) + i * self.vertical_f
            elif self.function == 1:
                y = np.exp(x) + i * self.vertical_f
            elif self.function == 2:
                y = np.random.random(x.shape) + i * self.vertical_f
            else:
                y = i * self.vertical_f * np.ones(x.shape)
            if self.plotItemContainer[i] is not None:
                self.plotItemContainer[i].clear()
                self.plotItemContainer[i].setData(x, y)
            else:
                self.plotItemContainer[i] = self.pltWidget.plot(x, y,
                                                                name=f"plot {i}",
                                                                pen=pg.mkPen(width=3, color=i))
                                                                # pen=(i, self.n_plots))
        t2 = time()
        print("Number of plots: ", self.n_plots)
        print(f"time: {t2-t1}s")

    def saveFig(self, filename):
        # create an exporter instance, as an argument give it
        # the item you wish to export
        exporter = pg.exporters.ImageExporter(self.pltWidget.plotItem)

        # save to file
        exporter.export(filename)


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    app = QApplication(sys.argv)
    w = MainWindow()
    w.plotData()
    # w.saveFig('benchmarkfigure_pyqtgraph.png')
    w.show()
    app.exec_()
