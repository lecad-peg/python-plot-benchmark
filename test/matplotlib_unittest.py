"""
Unittest for matplotlib benchmark case.

Usage: either
    1.) run this Python file normally
    2.) run in terminal "python3 -m unittest -v matplotlib_unittest -b"

Requirements:

- pip3 install pillow
- pip3 install imagehash

Notes:
    https://docs.python.org/3.7/library/unittest.html
    https://docs.python.org/3.7/library/unittest.html#assert-methods

"""

import sys
import unittest
from time import time
import logging
import numpy as np
from PIL import Image
import imagehash
from PyQt5.QtWidgets import QApplication
sys.path.append("..")
from matplotlib_benchmark import *

# Add logger to enable "unittest" custom prints while --buffer=True is enabled
logger = logging.getLogger()
logger.level = logging.INFO
stream_handler = logging.StreamHandler(sys.stdout)
# Overwrite/suppress newline
stream_handler.terminator = " | "
logger.addHandler(stream_handler)

# Running QApp is required before running tests if we want to run
# "python3 -m unittest -v matplotlib_unittest -b"
app = QApplication(sys.argv)


class TestMatplotlib(unittest.TestCase):

    def setUp(self):
        self.startTime = time()
        self.ps = PlotSettings()

    def tearDown(self):
        t = time() - self.startTime
        # logger.info('%s: %.3f' % (self.id(), t))
        logger.info('%.3fs' % (t))

    def test_numberOfPlots(self):
        self.assertEqual(self.ps.numberOfPlots.value(), 10)
        self.assertEqual(self.ps.numberOfPlots.minimum(), 1)
        self.assertEqual(self.ps.numberOfPlots.maximum(), 100)
        self.assertEqual(self.ps.numberOfPlots.singleStep(), 1)

    def test_numberOfPointsPerPlot(self):
        self.assertEqual(self.ps.numberOfPointsPerPlot.value(), 100000)
        self.assertEqual(self.ps.numberOfPointsPerPlot.minimum(), 10)
        self.assertEqual(self.ps.numberOfPointsPerPlot.maximum(), 10000000)
        self.assertEqual(self.ps.numberOfPointsPerPlot.singleStep(), 100)

    def test_selectFunction(self):
        self.assertGreater(self.ps.selectFunction.count(), 1)
        self.assertEqual(self.ps.selectFunction.currentIndex(), 0)

    def test_verticalDisplacementVector(self):
        self.assertEqual(self.ps.verticalDisplacementFactor.value(), 2)
        self.assertEqual(self.ps.verticalDisplacementFactor.minimum(), 1)
        self.assertEqual(self.ps.verticalDisplacementFactor.maximum(), 10)
        self.assertEqual(self.ps.verticalDisplacementFactor.singleStep(), 1)

    def test_plot(self):
        w = MainWindow()
        w.plotData()

        x_correct = np.linspace(0, 12 * np.pi, self.ps.numberOfPointsPerPlot.value())
        x = w.canvas.axes.lines[0].get_xdata() # x values are the same for all lines
        np.testing.assert_equal(x_correct, x)

        for i in range(len(w.canvas.axes.lines)): # sin
            y_correct = np.sin(x_correct) + i * self.ps.verticalDisplacementFactor.value()

            y = w.canvas.axes.lines[i].get_ydata()
            np.testing.assert_equal(y_correct, y)

    def test_figureCheck(self):
        # Compare confirmed testing figure and the new figure.

        w = MainWindow()
        # w.n_plots = 20 # to manually produce error

        w.plotData()
        w.saveFig('testfigure_matplotlib_new.png')
        hash0 = imagehash.average_hash(Image.open('testfigure_matplotlib.png'))
        hash1 = imagehash.average_hash(Image.open('testfigure_matplotlib_new.png'))
        cutoff = 1

        self.assertTrue(hash0 - hash1 < cutoff)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestMatplotlib)
    unittest.TextTestRunner(verbosity=2, buffer=True).run(suite)
    # app.exec_()